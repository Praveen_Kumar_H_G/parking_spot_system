package com.training.parkingsystem.service;

import java.time.LocalDate;

import com.training.parkingsystem.dto.ParkingSpotResponseDto;

public interface SearchParkingSpotService {

	ParkingSpotResponseDto searchParkingSpot(long sapId, LocalDate fromDate, LocalDate toDate, int pageNumber, int pageSize);

}
