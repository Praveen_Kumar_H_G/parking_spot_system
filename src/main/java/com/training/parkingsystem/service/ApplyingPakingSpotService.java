package com.training.parkingsystem.service;

import com.training.parkingsystem.dto.ApiResponse;
import com.training.parkingsystem.dto.AppliedSpotDto;

public interface ApplyingPakingSpotService {

	ApiResponse applyNewPArkingSpots( AppliedSpotDto appliedSpotDto);

}
