package com.training.parkingsystem.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.training.parkingsystem.dto.ParkingSpotDto;
import com.training.parkingsystem.dto.ParkingSpotResponseDto;
import com.training.parkingsystem.entity.Employee;
import com.training.parkingsystem.entity.ParkingSpot;
import com.training.parkingsystem.exception.DateMissmatchException;
import com.training.parkingsystem.exception.EmployeeNotFound;
import com.training.parkingsystem.exception.ParkingSpotDetailsNotFound;
import com.training.parkingsystem.repository.EmployeeRepository;
import com.training.parkingsystem.repository.ParkingSpotRepository;
import com.training.parkingsystem.service.SearchParkingSpotService;
import com.training.parkingsystem.utill.ErrorConstant;
import com.training.parkingsystem.utill.SuccessConstant;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author praveenakumara.hitne
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class SearchParkingSpotServiceImpl implements SearchParkingSpotService {

	private final ParkingSpotRepository parkingSpotRepository;
	private final EmployeeRepository employeeRepository;

	/**
	 * searches for parking spot based on the employee's SAP ID and the date range.
	 * 
	 * This method will return a response containing the success status, a message
	 * and a list of available parking spots within the given date range. It
	 * supports pagination for efficient data retrieval.
	 * 
	 * @param sapId      The SAP ID of the Employee for the parking spot search.
	 * @param fromDate   The start date for the parking spot search.
	 * @param toDate     The end date for the parking spot search.
	 * @param pageNumber The page number for pagination.
	 * @param pageSize   The number of records per page.
	 * @return ParkingSpotResponseDto containing the success code, message and a
	 *         list of parking spots within the specified date range.
	 */

	@Override
	public ParkingSpotResponseDto searchParkingSpot(long sapId, LocalDate fromDate, LocalDate toDate, int pageNumber,
			int pageSize) {
		Optional<Employee> bySapId = employeeRepository.findBySapId(sapId);
		if (bySapId.isEmpty()) {
			log.error("Employee sap ID Invalid");
			throw new EmployeeNotFound();
		} else if (fromDate.isBefore(LocalDate.now())) {
			throw new DateMissmatchException(ErrorConstant.FROM_DATE_MISS_MATCH_EXCEPTION_MESSAGE,
					ErrorConstant.DATE_MISS_MATCH_EXCEPTION_CODE);
		} else if (toDate.isBefore(LocalDate.now())) {
			throw new DateMissmatchException(ErrorConstant.TO_DATE_MISS_MATCH_EXCEPTION_MESSAGE,
					ErrorConstant.DATE_MISS_MATCH_EXCEPTION_CODE);
		}

		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<ParkingSpot> fromDateBetween = parkingSpotRepository.findByFromDateBetween(fromDate, toDate, pageable);
		if (fromDateBetween.getContent().isEmpty()) {
			throw new ParkingSpotDetailsNotFound(ErrorConstant.PARKING_SPOT_DETAILS_NOT_FOUND_MESSAGE,
					ErrorConstant.PARKING_SPOT_DETAILS_NOT_FOUND_CODE);
		}

		List<ParkingSpotDto> spotDtos = fromDateBetween.stream()
				.map(parkingSpot -> ParkingSpotDto.builder().spotNumber(parkingSpot.getSpotNumber())
						.availableTo(parkingSpot.getToDate()).availableFrom(parkingSpot.getFromDate())
						.status(parkingSpot.getStatus()).build())
				.toList();

		return ParkingSpotResponseDto.builder()
				.statusMessage(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE)
				.statusCode(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_CODE).parkingSpotDtos(spotDtos).build();

	}

}
