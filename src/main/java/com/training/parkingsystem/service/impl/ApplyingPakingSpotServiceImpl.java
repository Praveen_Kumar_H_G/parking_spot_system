package com.training.parkingsystem.service.impl;

import org.springframework.stereotype.Service;

import com.training.parkingsystem.dto.ApiResponse;
import com.training.parkingsystem.dto.AppliedSpotDto;
import com.training.parkingsystem.entity.AppliedSpot;
import com.training.parkingsystem.entity.Employee;
import com.training.parkingsystem.entity.ParkingSpot;
import com.training.parkingsystem.entity.Status;
import com.training.parkingsystem.exception.EmployeeNotFound;
import com.training.parkingsystem.exception.ParkingSpotDetailsNotFound;
import com.training.parkingsystem.repository.AppliedSpotRepository;
import com.training.parkingsystem.repository.EmployeeRepository;
import com.training.parkingsystem.repository.ParkingSpotRepository;
import com.training.parkingsystem.service.ApplyingPakingSpotService;
import com.training.parkingsystem.utill.ErrorConstant;
import com.training.parkingsystem.utill.SuccessConstant;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author praveenakumara.hitne
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ApplyingPakingSpotServiceImpl implements ApplyingPakingSpotService {

	private final EmployeeRepository employeeRepository;
	private final ParkingSpotRepository parkingSpotRepository;
	private final AppliedSpotRepository appliedSpotRepository;

	/**
	 * Applying Parking spot using employee SAP ID and parking spots number.
	 * 
	 * @param appliedSpotDto containing the employee SAP ID and Parking spots
	 *                       number.
	 * @return ApiResponse containing the success code and success message.
	 */

	@Override
	public ApiResponse applyNewPArkingSpots(AppliedSpotDto appliedSpotDto) {
		Employee employee = employeeRepository.findBySapId(appliedSpotDto.sapId()).orElseThrow(() -> {
			log.error("Invalid Employee SAP ID.");
			throw new EmployeeNotFound();
		});

		ParkingSpot parkingSpot = parkingSpotRepository.findBySpotNumberAndFromDateBetween(appliedSpotDto.spotNumber(),
				appliedSpotDto.fromDate(), appliedSpotDto.toDate()).orElseThrow(() -> {
					log.error("Parking spot number details not found for this date.");
					throw new ParkingSpotDetailsNotFound(
							ErrorConstant.PARKING_SPOT_DETAILS_NOT_FOUND_FOR_THIS_DATE_MESSAGE,
							ErrorConstant.PARKING_SPOT_DETAILS_NOT_FOUND_FOR_THIS_DATE_CODE);
				});

		if (parkingSpot.getStatus().equals(Status.UNAVAILABLE)) {
			log.error("Parking spot is already booked in this date.");
			throw new ParkingSpotDetailsNotFound(ErrorConstant.PARKING_SPOT_DETAILS_UNAVAILABLE_MESSAGE,
					ErrorConstant.PARKING_SPOT_DETAILS_UNAVAILABLE_CODE);
		}

		AppliedSpot appliedSpot = new AppliedSpot();
		appliedSpot.setEmployee(employee);
		appliedSpot.setParkingSpot(parkingSpot);
		appliedSpot.setStatus(Status.PENDING);
		log.info("saving the applied statements");
		appliedSpotRepository.save(appliedSpot);
		parkingSpot.setStatus(Status.UNAVAILABLE);
		log.info("changing the status parking spot after applied");
		parkingSpotRepository.save(parkingSpot);
		log.info("success code and message display");
		return ApiResponse.builder().statusCode(SuccessConstant.APPLIED_SUCCESSFULL_CODE)
				.message(SuccessConstant.APPLIED_SUCCESSFULL_MESSAGE).build();

	}

}
