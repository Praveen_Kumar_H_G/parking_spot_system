package com.training.parkingsystem.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "parking_allocations")
@Data
@Builder
public class ParkingAllocation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long allocationId;
	@OneToOne
	private ParkingSpot parkingSpot;
	@OneToOne
	private Employee employee;
	

}
