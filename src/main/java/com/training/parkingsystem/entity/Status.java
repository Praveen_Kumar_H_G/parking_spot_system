package com.training.parkingsystem.entity;

public enum Status {
	AVAILABLE,UNAVAILABLE,PENDING,BOOKED
}
