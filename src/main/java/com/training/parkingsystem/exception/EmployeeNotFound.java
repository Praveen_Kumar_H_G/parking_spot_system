package com.training.parkingsystem.exception;

import com.training.parkingsystem.utill.ErrorConstant;

public class EmployeeNotFound extends GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmployeeNotFound() {
		super(ErrorConstant.EMPLOYEE_NOT_FOUND_MESSAGE, ErrorConstant.EMPLOYEE_NOT_FOUND_CODE);
	}
	
	

}
