package com.training.parkingsystem.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		ValidationDto validationDto = new ValidationDto();
		validationDto.setValidationErrors(errors);
		validationDto.setErrorCode("FSA0003");
		validationDto.setErrorMessage("Please add valid input data");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationDto);
	}

	@ExceptionHandler(EmployeeNotFound.class)
	protected ResponseEntity<ErrorResponse> handleGlobalException(EmployeeNotFound employeeNotFound) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder().code(employeeNotFound.getCode())
				.message(employeeNotFound.getMessage()).build());
	}

	@ExceptionHandler(ParkingSpotDetailsNotFound.class)
	protected ResponseEntity<ErrorResponse> handleGlobalException(
			ParkingSpotDetailsNotFound parkingSpotDetailsNotFound) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(parkingSpotDetailsNotFound.getCode()).message(parkingSpotDetailsNotFound.getMessage()).build());
	}

	@ExceptionHandler(DateMissmatchException.class)
	protected ResponseEntity<ErrorResponse> handleGlobalException(DateMissmatchException dateMissmatchException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(dateMissmatchException.getCode()).message(dateMissmatchException.getMessage()).build());
	}
}
