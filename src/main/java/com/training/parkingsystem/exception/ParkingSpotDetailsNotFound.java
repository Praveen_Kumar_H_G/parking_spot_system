package com.training.parkingsystem.exception;

public class ParkingSpotDetailsNotFound extends GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParkingSpotDetailsNotFound(String message, String code) {
		super(message, code);
	}

}
