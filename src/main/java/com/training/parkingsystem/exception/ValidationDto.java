package com.training.parkingsystem.exception;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ValidationDto {
	
	private Map<String, String> validationErrors;
	private String errorCode;
	private String errorMessage;

}
