package com.training.parkingsystem.exception;

public class DateMissmatchException extends GlobalException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateMissmatchException(String message, String statusCode) {
		super(message,statusCode);
	}
	
	

}
