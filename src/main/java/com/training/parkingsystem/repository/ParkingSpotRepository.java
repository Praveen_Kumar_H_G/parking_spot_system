package com.training.parkingsystem.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.training.parkingsystem.entity.ParkingSpot;

public interface ParkingSpotRepository extends JpaRepository<ParkingSpot, Long> {

	Page<ParkingSpot> findByFromDateBetween(LocalDate fromDate, LocalDate toDate, Pageable pageable);

	Optional<ParkingSpot> findBySpotNumberAndFromDateBetween(String spotNumber, LocalDate fromDate, LocalDate toDate);

}
