package com.training.parkingsystem.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.parkingsystem.entity.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	Optional<Employee> findBySapId(long sapId);

}
