package com.training.parkingsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.parkingsystem.entity.AppliedSpot;

public interface AppliedSpotRepository extends JpaRepository<AppliedSpot, Long> {

}
