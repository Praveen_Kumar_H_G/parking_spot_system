package com.training.parkingsystem.controller;

import java.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.parkingsystem.dto.ParkingSpotResponseDto;
import com.training.parkingsystem.service.SearchParkingSpotService;

import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 *@author praveenakumara.hitne 
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Slf4j
public class SearchParkingSpotController {

	private final SearchParkingSpotService searchParkingSpotService;

	/**
	 * searches for parking spot based on the employee's SAP ID and the date range.
	 * 
	 * This method will return a response containing the success status, a message and a list of available parking spots within the given date range.
	 * It supports pagination for efficient data retrieval.
	 *  
	 * @param sapId the SAP ID of the Employee for the parking spot search.
	 * @param fromDate The start date for the parking spot search.
	 * @param toDate The end date for the parking spot search.
	 * @param pageNumber The page number for pagination.
	 * @param pageSize The number of records per page.
	 * @return ParkingSpotResponseDto containing the  success code, message and a list of parking spots within the specified date range.
	 */
	
	@GetMapping("/parking-spots")
	public ResponseEntity<ParkingSpotResponseDto> searchParkingSpot(@RequestParam("SAPId") long sapId,
			@RequestParam("fromDate") LocalDate fromDate, @RequestParam("toDate") LocalDate toDate,
			@RequestParam(required = false, defaultValue = "0") int pageNumber,
			@Min(value = 1) @RequestParam(required = false, defaultValue = "2") int pageSize) {
		log.info("Search parking spot controller statred");
		return new ResponseEntity<>(
				searchParkingSpotService.searchParkingSpot(sapId, fromDate, toDate, pageNumber, pageSize),
				HttpStatus.OK);
	}

}
