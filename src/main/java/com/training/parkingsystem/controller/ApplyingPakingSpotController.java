package com.training.parkingsystem.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.parkingsystem.dto.ApiResponse;
import com.training.parkingsystem.dto.AppliedSpotDto;
import com.training.parkingsystem.service.ApplyingPakingSpotService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * @author praveenakumara.hitne
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class ApplyingPakingSpotController {
	
	private final ApplyingPakingSpotService applyingPakingSpotService;
	
	/**
	 * Applying Parking spot using employee SAP ID and parking spots number.
	 * 
	 * @param appliedSpotDto containing the employee SAP ID and Parking spots number. 
	 * @return ApiResponse containing the success code and success message. 
	 */
	
	@PostMapping("/applied-spots")
	public ResponseEntity<ApiResponse> applingNewParkingSpot( @Valid @RequestBody AppliedSpotDto appliedSpotDto){
		log.info("Applying parking spot controller started.");
		return new ResponseEntity<>(applyingPakingSpotService.applyNewPArkingSpots(appliedSpotDto),HttpStatus.CREATED);
		
	}

}
