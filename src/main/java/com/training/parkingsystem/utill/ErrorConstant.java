package com.training.parkingsystem.utill;

public interface ErrorConstant {
	
	String EMPLOYEE_NOT_FOUND_CODE="EX0001";
	String EMPLOYEE_NOT_FOUND_MESSAGE="Inavalid empolyee SAP Id.";
	
	String DATE_MISS_MATCH_EXCEPTION_CODE = "EX0002";
	String FROM_DATE_MISS_MATCH_EXCEPTION_MESSAGE = "From date is not valid.";
	String TO_DATE_MISS_MATCH_EXCEPTION_MESSAGE = "To date is not valid.";

	
	String PARKING_SPOT_DETAILS_NOT_FOUND_CODE = "EX0003";
	String PARKING_SPOT_DETAILS_NOT_FOUND_MESSAGE = "Parking spot details not found in this date.";
	
	String PARKING_SPOT_DETAILS_UNAVAILABLE_CODE = "EX0006";
	String PARKING_SPOT_DETAILS_UNAVAILABLE_MESSAGE = "Parking spot is already booked in this date.";
	
	String PARKING_SPOT_NUMBER_NOT_FOUND_CODE = "EX0004";
	String PARKING_SPOT_NUMBER_NOT_FOUND_MESSAGE = "Parking spot number not found.";
	
	String PARKING_SPOT_DETAILS_NOT_FOUND_FOR_THIS_DATE_MESSAGE="Parking spot number details not found for this date.";
	String PARKING_SPOT_DETAILS_NOT_FOUND_FOR_THIS_DATE_CODE="EX0005";
}
