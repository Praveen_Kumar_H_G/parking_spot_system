package com.training.parkingsystem.utill;

public interface SuccessConstant {
	
	String ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE = "Successfully Executed";
	String ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_CODE = "SUCCESS-1";
	
	String APPLIED_SUCCESSFULL_CODE="SUCCESS-001";
	String APPLIED_SUCCESSFULL_MESSAGE="Applied successfully.";

}
