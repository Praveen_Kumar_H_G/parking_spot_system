package com.training.parkingsystem.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

@Builder
public record AppliedSpotDto(
		@Min( value = 1, message="Employee SAP ID must be at least 6 digits long.") 
		long sapId,
		@NotBlank(message="Parking spot number required.") String spotNumber,
		LocalDate fromDate, LocalDate toDate) {

}
