package com.training.parkingsystem.dto;

import java.time.LocalDate;

import com.training.parkingsystem.entity.Status;

import lombok.Builder;

@Builder
public record ParkingSpotDto(String spotNumber, 
		Status status, 
		LocalDate availableFrom, 
		LocalDate availableTo) {

}
