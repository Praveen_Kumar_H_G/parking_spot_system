package com.training.parkingsystem.dto;

import java.util.List;

import lombok.Builder;

@Builder
public record ParkingSpotResponseDto(String statusCode, String statusMessage,
		List<ParkingSpotDto> parkingSpotDtos) {

}
