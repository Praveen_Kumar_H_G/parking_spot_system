package com.training.parkingsystem.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String statusCode,
		String message) {

}
