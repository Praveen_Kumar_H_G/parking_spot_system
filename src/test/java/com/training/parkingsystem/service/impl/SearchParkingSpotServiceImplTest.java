package com.training.parkingsystem.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.parkingsystem.dto.ParkingSpotResponseDto;
import com.training.parkingsystem.entity.Employee;
import com.training.parkingsystem.entity.ParkingSpot;
import com.training.parkingsystem.entity.Status;
import com.training.parkingsystem.exception.DateMissmatchException;
import com.training.parkingsystem.exception.EmployeeNotFound;
import com.training.parkingsystem.exception.ParkingSpotDetailsNotFound;
import com.training.parkingsystem.repository.EmployeeRepository;
import com.training.parkingsystem.repository.ParkingSpotRepository;
import com.training.parkingsystem.utill.ErrorConstant;
import com.training.parkingsystem.utill.SuccessConstant;

@ExtendWith(SpringExtension.class)
class SearchParkingSpotServiceImplTest {

	@Mock
	ParkingSpotRepository parkingSpotRepository;

	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	Pageable pageable;

	@InjectMocks
	SearchParkingSpotServiceImpl searchParkingSpotServiceImpl;

	long sapId = 1223213l;
	LocalDate fromDate = LocalDate.now().minusDays(2);
	LocalDate toDate = LocalDate.now().minusDays(2);

	LocalDate fromDate1 = LocalDate.now();
	LocalDate toDate1 = LocalDate.now();

	Employee emp = Employee.builder().sapId(sapId).build();

	@Test
	void employeeNotFoundTest() {
		Mockito.when(employeeRepository.findBySapId(sapId)).thenReturn(Optional.empty());
		assertThrows(EmployeeNotFound.class,
				() -> searchParkingSpotServiceImpl.searchParkingSpot(sapId, fromDate, toDate, 0, 2));
	}

	@Test
	void fromDateNotVaildTest() {
		List<ParkingSpot> listOFParkingSpot = new ArrayList<>();
		ParkingSpot spot = ParkingSpot.builder().parkingId(1l).fromDate(fromDate).toDate(toDate)
				.status(Status.AVAILABLE).spotNumber("123456kh").build();
		listOFParkingSpot.add(spot);
		Mockito.when(employeeRepository.findBySapId(sapId)).thenReturn(Optional.of(emp));
		Pageable pageable = Mockito.mock(Pageable.class);
		PageImpl<ParkingSpot> pageImpl = new PageImpl<>(listOFParkingSpot, pageable, listOFParkingSpot.size());
		Mockito.when(parkingSpotRepository.findByFromDateBetween(fromDate, toDate, pageable)).thenReturn(pageImpl);
		assertThrows(DateMissmatchException.class,
				() -> searchParkingSpotServiceImpl.searchParkingSpot(sapId, fromDate, toDate, 0, 2));
	}

	@Test
	void toDateNotVaildTest() {
		List<ParkingSpot> listOFParkingSpot = new ArrayList<>();
		ParkingSpot spot = ParkingSpot.builder().parkingId(1l).fromDate(LocalDate.now()).toDate(LocalDate.now())
				.status(Status.AVAILABLE).spotNumber("123456kh").build();
		listOFParkingSpot.add(spot);
		Mockito.when(employeeRepository.findBySapId(sapId)).thenReturn(Optional.of(emp));
		Pageable pageable = Mockito.mock(Pageable.class);
		PageImpl<ParkingSpot> pageImpl = new PageImpl<>(listOFParkingSpot, pageable, listOFParkingSpot.size());
		Mockito.when(parkingSpotRepository.findByFromDateBetween(fromDate1, toDate, pageable))
				.thenReturn(pageImpl);
		DateMissmatchException exception = assertThrows(DateMissmatchException.class,
				() -> searchParkingSpotServiceImpl.searchParkingSpot(sapId, fromDate1, toDate, 0, 2));
		assertEquals(ErrorConstant.TO_DATE_MISS_MATCH_EXCEPTION_MESSAGE, exception.getMessage());
	}

	@Test
	void noRecordNotFounTest() {
		Mockito.when(employeeRepository.findBySapId(sapId)).thenReturn(Optional.of(emp));
		Mockito.when(parkingSpotRepository.findByFromDateBetween(any(), any(), any(Pageable.class))).thenReturn(Page.empty());
		assertThrows(ParkingSpotDetailsNotFound.class,
				() -> searchParkingSpotServiceImpl.searchParkingSpot(sapId, fromDate1, toDate1, 0, 2));

	}
	
	@Test
	void searchParkingSpotTest() {
		List<ParkingSpot> listOFParkingSpot = new ArrayList<>();
		ParkingSpot spot = ParkingSpot.builder().parkingId(1l).fromDate(fromDate1).toDate(toDate1)
				.status(Status.AVAILABLE).spotNumber("123456kh").build();
		listOFParkingSpot.add(spot);
		Mockito.when(employeeRepository.findBySapId(sapId)).thenReturn(Optional.of(emp));
		Pageable pageable = Mockito.mock(Pageable.class);
		PageImpl<ParkingSpot> pageImpl = new PageImpl<>(listOFParkingSpot, pageable, listOFParkingSpot.size());
		Mockito.when(parkingSpotRepository.findByFromDateBetween(any(), any(), any(Pageable.class))).thenReturn(pageImpl);
		ParkingSpotResponseDto parkingSpot = searchParkingSpotServiceImpl.searchParkingSpot(sapId, fromDate1, toDate1, 0, 2);
		assertEquals(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE, parkingSpot.statusMessage());
		
	}

}
