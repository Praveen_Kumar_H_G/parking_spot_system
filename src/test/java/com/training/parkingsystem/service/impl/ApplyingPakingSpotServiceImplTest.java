package com.training.parkingsystem.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.parkingsystem.dto.ApiResponse;
import com.training.parkingsystem.dto.AppliedSpotDto;
import com.training.parkingsystem.entity.Employee;
import com.training.parkingsystem.entity.ParkingSpot;
import com.training.parkingsystem.entity.Status;
import com.training.parkingsystem.exception.EmployeeNotFound;
import com.training.parkingsystem.exception.ParkingSpotDetailsNotFound;
import com.training.parkingsystem.repository.AppliedSpotRepository;
import com.training.parkingsystem.repository.EmployeeRepository;
import com.training.parkingsystem.repository.ParkingSpotRepository;
import com.training.parkingsystem.utill.SuccessConstant;

@ExtendWith(SpringExtension.class)
class ApplyingPakingSpotServiceImplTest {
	
	@Mock
	ParkingSpotRepository parkingSpotRepository;
	
	@Mock
	EmployeeRepository employeeRepository;
	
	@Mock
	AppliedSpotRepository appliedSpotRepository;
	
	@InjectMocks
	ApplyingPakingSpotServiceImpl applyingPakingSpotServiceImpl;
	
	long empSAPID= 123456l;
	String spotNumbers= "11HGP";
	LocalDate stratDate= LocalDate.now();
	LocalDate endDate = LocalDate.now();
	
	ParkingSpot parkingSpot= ParkingSpot.builder()
			.fromDate(stratDate)
			.toDate(endDate)
			.status(Status.AVAILABLE)
			.spotNumber(spotNumbers)
			.build();
	
	ParkingSpot parkingSpot1= ParkingSpot.builder()
			.fromDate(stratDate)
			.toDate(endDate)
			.status(Status.UNAVAILABLE)
			.spotNumber(spotNumbers)
			.build();
	
	AppliedSpotDto spotDto = AppliedSpotDto.builder()
			.fromDate(LocalDate.now())
			.toDate(LocalDate.now())
			.spotNumber(spotNumbers)
			.sapId(empSAPID)
			.build();
	
	Employee emp = Employee.builder()
			.sapId(empSAPID).build();
	
	
	@Test
	void employeeNotFoundTest() {
		Mockito.when(employeeRepository.findBySapId(empSAPID)).thenReturn(Optional.empty());
		assertThrows(EmployeeNotFound.class, ()-> applyingPakingSpotServiceImpl.applyNewPArkingSpots(spotDto));
	}
	@Test
	void parkingSpotNumberDetailsNotFound() {
		Mockito.when(employeeRepository.findBySapId(empSAPID)).thenReturn(Optional.of(emp));
		Mockito.when(parkingSpotRepository.findBySpotNumberAndFromDateBetween(spotNumbers, LocalDate.now(), LocalDate.now())).thenReturn(Optional.empty());
		assertThrows(ParkingSpotDetailsNotFound.class, ()-> applyingPakingSpotServiceImpl.applyNewPArkingSpots(spotDto));
	}
	
	@Test
	void parkingSpotUnAvailableTest() {
		Mockito.when(employeeRepository.findBySapId(empSAPID)).thenReturn(Optional.of(emp));
		Mockito.when(parkingSpotRepository.findBySpotNumberAndFromDateBetween(spotNumbers, stratDate, endDate)).thenReturn(Optional.of(parkingSpot1));
		assertThrows(ParkingSpotDetailsNotFound.class, ()-> applyingPakingSpotServiceImpl.applyNewPArkingSpots(spotDto));
	}
	@Test 
	void parkingSpotAppliedSuccessTest() {
		Mockito.when(employeeRepository.findBySapId(empSAPID)).thenReturn(Optional.of(emp));
		Mockito.when(parkingSpotRepository.findBySpotNumberAndFromDateBetween(spotNumbers, stratDate, endDate)).thenReturn(Optional.of(parkingSpot));
		ApiResponse applyNewPArkingSpots = applyingPakingSpotServiceImpl.applyNewPArkingSpots(spotDto);
		assertEquals(SuccessConstant.APPLIED_SUCCESSFULL_MESSAGE, applyNewPArkingSpots.message());
	}
	

}


