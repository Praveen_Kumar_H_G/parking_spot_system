package com.training.parkingsystem.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.parkingsystem.dto.ParkingSpotResponseDto;
import com.training.parkingsystem.service.SearchParkingSpotService;
import com.training.parkingsystem.utill.SuccessConstant;

@ExtendWith(SpringExtension.class)
class SearchParkingSpotControllerTest {
	
	@Mock
	SearchParkingSpotService searchParkingSpotService;
	
	@InjectMocks
	SearchParkingSpotController searchParkingSpotController;
	
	long sapId= 12345678l;
	LocalDate fromDate = LocalDate.now();
	LocalDate toDate= LocalDate.now();
	
	ParkingSpotResponseDto dto = ParkingSpotResponseDto.builder().statusCode(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_CODE)
			.statusMessage(SuccessConstant.ACCOUNT_DETAILS_FETCHED_SUCCESSFULL_MESSAGE)
			.parkingSpotDtos(null).build();
	@Test
	void searchParkingSpotsTest() {
	Mockito.when(searchParkingSpotService.searchParkingSpot(sapId, fromDate, toDate, 0, 2)).thenReturn(dto);
	ResponseEntity<ParkingSpotResponseDto> searchParkingSpot = searchParkingSpotController.searchParkingSpot(sapId, fromDate, toDate, 0, 2);
	assertEquals(HttpStatus.OK, searchParkingSpot.getStatusCode());
	}

}
