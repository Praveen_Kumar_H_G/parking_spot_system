package com.training.parkingsystem.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.parkingsystem.dto.ApiResponse;
import com.training.parkingsystem.dto.AppliedSpotDto;
import com.training.parkingsystem.service.ApplyingPakingSpotService;
import com.training.parkingsystem.utill.SuccessConstant;

@ExtendWith(SpringExtension.class)
class ApplyingPakingSpotControllerTest {
	@Mock
	ApplyingPakingSpotService applyingPakingSpotService;
	
	@InjectMocks
	ApplyingPakingSpotController applyingPakingSpotController;
	
	ApiResponse apiResponse = ApiResponse.builder()
			.statusCode(SuccessConstant.APPLIED_SUCCESSFULL_CODE)
			.message(SuccessConstant.APPLIED_SUCCESSFULL_MESSAGE).build();
	
	long empSAPID= 123456l;
	String spotNumbers= "11HGP";
	LocalDate stratDate= LocalDate.now();
	LocalDate endDate = LocalDate.now();
	
	AppliedSpotDto spotDto = AppliedSpotDto.builder()
			.fromDate(LocalDate.now())
			.toDate(LocalDate.now())
			.spotNumber(spotNumbers)
			.sapId(empSAPID)
			.build();
	@Test
	void applyingParkingSpotSuccessTest() {
		Mockito.when(applyingPakingSpotService.applyNewPArkingSpots(spotDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> parkingSpot = applyingPakingSpotController.applingNewParkingSpot(spotDto);
		assertEquals(HttpStatus.CREATED, parkingSpot.getStatusCode());
	}
}
